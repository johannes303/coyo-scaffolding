# Migrating to GitLab

https://gitlab.com/johannes303


# COYO Scaffolding Project

This repository contains a scaffolding project setup for a COYO 4 project.
It is intended to be forked by partners and customers who want to customize COYO.

For details, please check out the [COYO developer guides](http://docs.coyoapp.com).

## Development

### 1. Start Docker containers

- To let docker read the '.env' file which contains all environment variables, switch to the dev-tools folder:

    ```
    cd tools/dev
    ```

- Change the COYO version in the .env file if needed:

    ```
    COYO_VERSION=<VERSION>-<BUILD>
    ```

Now jump to one of the following steps:

#### 1.1. Full-stack development

If you want to develop full-stack (backend and frontend), execute "start-dev.sh":

```
./start-dev.sh
```

or manually start the "docker-compose.dev.yml" script:

```
docker-compose -f docker-compose.dev.yml up
```

This will start the needed docker containers and you can start the backend and frontend on your own.

#### 1.2. Backend-only development

If you want to develop backend-only, start the "docker-compose.dev.frontend.yml" script.

```
docker-compose -f docker-compose.dev.frontend.yml up
```

This will start the needed docker containers including the frontend container and you can start the backend on your own.

Demo data will be bootstrapped.

#### 1.3. Frontend-only development

If you want to develop frontend-only, start the "docker-compose.dev.backend.yml" script.

```
docker-compose -f docker-compose.dev.backend.yml up
```

This will start the needed docker containers including the backend container and you can start the frontend on your own.

Note: You may want to change "COYO_FRONTEND_URL" in the ".env" file to "localhost:3000".

### 2. Start backend

- Switch to the backend folder:

    ```
    cd backend
    ```

- Change the COYO version in the build.gradle file:

```
dependencyManagement {
    imports {
        mavenBom 'com.mindsmash.coyo:coyo-starter:<VERSION>-<BUILD>'
    }
}
```


```
dependencies {
    compile('com.mindsmash.coyo:coyo-starter:<VERSION>-<BUILD>')
    
    # ...
}
```

- Build and refresh gradle dependencies

- Run
    - Profile: "dev"

### 3. Start frontend

- Switch to the frontend folder:

    ```
    cd frontend
    ```

- Change the COYO version in the bower.json file:

    ```
    "dependencies": {
        "coyo": "git@bitbucket.org:coyoapp/coyo-bower.git#v<VERSION>-<BUILD>"
    }
    ```

- Install the dependencies:

    ```
    npm install
    ```

- Start the frontend server:

    ```
    gulp serve
    ```

- Or start the frontend server in distribution mode:

    ```
    gulp serve:dist
    ```


## Distribution

COYO 4 comes with a script that starts up all needed components for running the application.

It starts up a load balancer, a postgres database, redis, a message queue, the backend and frontend, the documentation
server and a Kibana logging server.

Before starting up, you need to package (and publish) your own docker images containing the adapted COYO.
After that, you simply have to adapt the docker-compose file to use the freshly created docker images.

The following section gives an overview on how to package, publish and use your own docker images.

### Packaging of the frontend

#### 1. Build and create the docker image

- Switch to the frontend folder:

    ```
    cd frontend
    ```

- Install the dependencies:

    ```
    npm install
    ```

- Build the frontend:

    ```
    gulp build
    ```

- Build the docker image:

    ```
    docker build -t <FrontendImageName>:<TageName> .
    ```

The last command will build a docker image named ```FrontendImageName```, e.g. ```my-company/my-custom-frontend```, with the tag ```TagName```, e.g. ```1.0.0-BETA.CUSTOM.1```.

#### 2. Publish image

- Push the newly created docker image to a service, e.g. to [Docker Hub](https://hub.docker.com)

    ```
    docker login -u=<USERNAME> -p=<PASSWORD>
    docker push <FrontendImageName>:<TageName>
    ```

#### 3. Adapt your docker-compose scripts to use your docker image

After publishing your custom image, you can adapt your docker-compose scripts to use them instead of the official COYO docker image.

Assuming you have customized, created and pushed a docker image ```my-company/my-custom-frontend``` with the tag ```1.0.0-BETA.CUSTOM.1```.

To use it, adapt your docker-compose scripts to use your customized docker image as follows:

```
coyo-frontend:
    image: my-company/my-custom-frontend:1.0.0-BETA.CUSTOM.1
    environment:
        - ...
```


### Packaging of the backend

#### 1. Build and create the docker image

- Switch to the backend folder:

    ```
    cd backend
    ```

- Build the backend:

    ```
    ./gradlew dist
    ```

- Build the docker image:

    ```
    docker build -t <BackendImageName>:<TageName> .
    ```

The last command will build a docker image named ```BackendImageName```, e.g. ```my-company/my-custom-backend```, with the tag ```TagName```, e.g. ```1.0.0-BETA.CUSTOM.1```.

#### 2. Publish image

- Push the newly created docker image to a service, e.g. to [Docker Hub](https://hub.docker.com)

    ```
    docker login -u=<USERNAME> -p=<PASSWORD>
    docker push <BackendImageName>:<TageName>
    ```

#### 3. Adapt your docker-compose scripts to use your docker image

After publishing your custom image, you can adapt your docker-compose scripts to use them instead of the official COYO docker image.

Assuming you have customized, created and pushed a docker image ```my-company/my-custom-backend``` with the tag ```1.0.0-BETA.CUSTOM.1```.

To use it, adapt your docker-compose scripts to use your customized docker image as follows:

```
coyo-backend:
    image: my-company/my-custom-backend:1.0.0-BETA.CUSTOM.1
    environment:
        - ...
```


### Test on local machine

#### 1. Add domain to etc/hosts

For the whole setup to be as similar as a production environment, virtual hosts are launched by docker.
For these virtual hosts it is necessary to specify a domain.
By default this domain is `http://local.coyo4.com`.
This domain must be added to your local `/etc/hosts` and point to your machines' ip address (or the docker VM machines alternatively).

```
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1             localhost
192.168.0.100   local.coyo4.com
```

#### 2. Generate an SSL certificate

The Enterprise Edition of COYO needs a certificate by default. A self-signed certificate can be created by running: 

```
openssl req -x509 -newkey rsa:2048 -keyout key.pem -out ca.pem -days 1080 -nodes -subj '/CN=*/O=mindsmash GmbH./C=DE'
cp key.pem cert.pem
cat ca.pem >> cert.pem
```

The cert.pem must be located in the same directory as the docker compose script (docker-compose.yml).

#### 3. Start docker-compose script

1. Include your own backend/frontend docker components in "docker-compose.yml"
2. Start up the components by running:

    ```
    docker-compose -f docker-compose.yml up
    ```

#### 4. Open Application(s) in your browser

You are now set and can open the application in your browser: [http://local.coyo4.com](http://local.coyo4.com)

**Note:** Additional tools are installed for you to use:

* Kibana Logstash: [http://dev:dev@local.coyo4.com:5601](http://dev:dev@local.coyo4.com:5601)
* Documentation: [http://local.coyo4.com/docs](http://local.coyo4.com/docs)
* Tenant-Management: [http://admin:admin@local.coyo4.com/tenants](http://admin:admin@local.coyo4.com/tenants)
