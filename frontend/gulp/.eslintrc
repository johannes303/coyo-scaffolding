{
  "root": true,
  "plugins": ["angular", "jasmine", "jsdoc"],
  "extends": ["eslint:recommended", "angular"],
  "env": {
    "browser": true,
    "angular/angular": true
  },
  "globals": {
    "_": true,
    "io": true,
    "jQuery": true, // use jQuery instead of $
    "Uint8Array": true,
    "Config": true,
    "StackTrace": true,
    "protocolCheck": true
  },
  "rules": {
    /* Possible Errors */

    /* Best Practices */
    "consistent-return": 2,
    "curly": 2,
    "dot-location":[2, "property"],
    "dot-notation": 2,
    "eqeqeq": 2,
    "no-eq-null": 2,
    "no-lone-blocks": 2,
    "no-multi-spaces": [2, {
      "ignoreEOLComments": true
    }],
    "no-multi-str": 2,
    "no-native-reassign": 2,
    "no-return-assign": 2,
    "no-self-compare": 2,
    "yoda": 2,

    /* Strict Mode */
    "strict": [2, "safe"],

    /* Stylistic Issues */
    "array-bracket-spacing": 1,
    "block-spacing": 1,
    "brace-style": [1, "1tbs", {
      "allowSingleLine": true
    }],
    "camelcase": 1,
    "comma-spacing": 1,
    "comma-style": 1,
    "computed-property-spacing": 1,
    "eol-last": 1,
    "indent": [1, 2, {
      "SwitchCase": 1,
      "VariableDeclarator": {
        "var": 2,
        "let": 2,
        "const": 3
      },
      "outerIIFEBody": 1,
      "MemberExpression": 2,
      "FunctionDeclaration": {
        "parameters": "first"
      },
      "FunctionExpression": {
        "parameters": "first"
      },
      "CallExpression": {
        "arguments": 2
      }
    }],
    "key-spacing": 1,
    "keyword-spacing": 1,
    "linebreak-style": 1,
    "new-cap": 1,
    "new-parens": 1,
    "no-lonely-if": 1,
    "no-mixed-spaces-and-tabs": 1,
    "no-trailing-spaces": 1,
    "object-curly-spacing": 1,
    "quotes": [1, "single"],
    "semi": 1,
    "semi-spacing": 1,
    "space-before-blocks": 1,
    "space-before-function-paren": [1, {
      "anonymous": "always",
      "named": "never"
    }],
    "space-in-parens": 1,
    "space-infix-ops": 1,
    "space-unary-ops": 1,

    /* Angular Plugin */
    "angular/on-watch": 0,
    "angular/service-name": [2, "/^[a-z]+/", {
      "oldBehavior":false
    }],
    "angular/factory-name": [2, "/(^[a-z]+)|(^[A-Z].*Model$)/"],
//    "angular/di-order": [1, true],

    /* Jasmine Plugin */
    "jasmine/no-focused-tests": 2,
    "jasmine/no-disabled-tests": 2,

    "jsdoc/require-param-type": 1,
    "jsdoc/require-returns-type": 1,
    "jsdoc/newline-after-description": 1,
//    "jsdoc/check-types": 1,
//    "jsdoc/require-param-description": 1,
//    "jsdoc/require-returns-description": 1,
//    "jsdoc/require-description-complete-sentence": 1,

    /* Custom Rules */
    "on": 2,
    "template-url": 2
  }
}
