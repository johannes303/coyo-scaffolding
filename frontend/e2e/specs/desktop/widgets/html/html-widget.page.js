(function () {
  'use strict';

  var extend = require('util')._extend;

  function HtmlWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      input: $('.html-widget-settings textarea')
    };

    api.content = {
      detailsTag: $('.widgets-container details'),
      summaryTag: $('.widgets-container summary')
    };

  }

  module.exports = HtmlWidget;

})();
