(function () {
  'use strict';

  var Select = require('../../../select.page.js');
  var Checkbox = require('../../../checkbox.page');

  var userDirectory = element(by.css('#remoteUserDirectory'));

  module.exports = {
    getById: function (id) {
      browser.get('/admin/user-management/users/edit/' + id);
    },
    getId: function () {
      browser.getCurrentUrl().then(function (url) {
        return /^.*\/admin\/user-management\/users\/edit\/([0-9-]+)/.exec(url);
      });
    },
    tabGeneral: element(
        by.css('.content .panel .nav-tabs *[translate="ADMIN.USER_MGMT.USERS.TABS.HEADINGS.GENERAL"]')),
    tabAdvanced: element(
        by.css('.content .panel .nav-tabs *[translate="ADMIN.USER_MGMT.USERS.TABS.HEADINGS.ADVANCED"]')),
    email: element(by.model('$ctrl.user.email')),
    firstname: element(by.model('$ctrl.user.firstname')),
    lastname: element(by.model('$ctrl.user.lastname')),
    roles: new Select(element(by.model('$ctrl.roles')), true),
    active: new Checkbox(element(by.model('$ctrl.user.active'))),
    password: element(by.model('$ctrl.user.password')),
    userDirectory: userDirectory,
    userDirectoryClear: userDirectory.$('.glyphicon-remove'),
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.css('.btn', 'Cancel'))
  };

})();
