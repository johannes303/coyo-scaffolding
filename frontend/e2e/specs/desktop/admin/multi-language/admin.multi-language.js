(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var multiLanguage = require('./admin.multi-language.page');

  describe('multi language administration', function () {

    beforeEach(function () {
      login.loginDefaultUser();
      multiLanguage.get();
    });

    it('should show navigation bar', function () {
      expect(multiLanguage.navigationBar.languagesButton.isDisplayed()).toBe(true);
      expect(multiLanguage.navigationBar.languagesButton.getText()).toBe('System languages');
      expect(multiLanguage.navigationBar.translationsButton.isDisplayed()).toBe(true);
      expect(multiLanguage.navigationBar.translationsButton.getText()).toBe('Interface translations');
    });
  });
})();
