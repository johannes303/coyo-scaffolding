(function () {
  'use strict';

  var Checkbox = require('../../../checkbox.page');

  module.exports = {
    connectionTab: {
      heading: $('uib-tab-heading span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.CONNECTION.HEADING"]'),
      host: element(by.model('$ctrl.ngModel.settings.host')),
      port: element(by.model('$ctrl.ngModel.settings.port')),
      ssl: new Checkbox(element(by.model('$ctrl.ngModel.settings.ssl'))),
      baseDn: element(by.model('$ctrl.ngModel.settings.baseDn')),
      username: element(by.model('$ctrl.ngModel.settings.username')),
      password: element(by.model('$ctrl.ngModel.settings.password'))
    },
    usersTab: {
      heading: $('uib-tab-heading span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.USER.HEADING"]'),
      userDn: element(by.model('$ctrl.ngModel.settings.userDn')),
      userObjectClass: element(by.model('$ctrl.ngModel.settings.userObjectClass')),
      userObjectFilter: element(by.model('$ctrl.ngModel.settings.userObjectFilter')),
      userId: element(by.model('$ctrl.ngModel.settings.userId')),
      userUsername: element(by.model('$ctrl.ngModel.settings.uusername')),
      userFirstName: element(by.model('$ctrl.ngModel.settings.userFirstName')),
      userLastName: element(by.model('$ctrl.ngModel.settings.userLastName')),
      userDisplayName: element(by.model('$ctrl.ngModel.settings.userDisplayName')),
      userEmail: element(by.model('$ctrl.ngModel.settings.userEmail')),
      userManager: element(by.model('$ctrl.ngModel.settings.userManager'))
    },
    groupsTab: {
      heading: $('uib-tab-heading span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.GROUP.HEADING"]'),
      enableGroupSync: element(by.model('$ctrl.ngModel.settings.groupSyncEnabled'))
    },
    syncTab: {
      heading: $('uib-tab-heading span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.SYNC.HEADING"]'),
      pageSize: element(by.model('$ctrl.ngModel.settings.pageSize')),
      followReferrals: new Checkbox(element(by.model('$ctrl.ngModel.settings.referals'))),
      activateNewUsers: new Checkbox(element(by.model('$ctrl.ngModel.settings.activation'))),
      justInTimeSync: new Checkbox(element(by.model('$ctrl.ngModel.settings.justInTime'))),
      deactivateOrphans: new Checkbox(element(by.model('$ctrl.ngModel.settings.deactivateOrphans'))),
      deleteOrphans: new Checkbox(element(by.model('$ctrl.ngModel.settings.deleteOrphans')))
    },
    schedulingTab: {
      heading: $('uib-tab-heading span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.SCHEDULING.HEADING"]')
    }
  };

})();
