
(function () {
  'use strict';

  function PageList() {
    var api = this;

    api.newButton = $('.actions-vertical a[ui-sref="main.page.create"]');
    api.searchInput = element(by.model('$ctrl.searchTerm'));
    api.search = function (pageName) {
      api.searchInput.sendKeys(pageName);
      return element.all(by.css('.page-card'));
    };
    api.filterAll = $('.panel-filterbox').element(by.css('li[text-key="MODULE.PAGES.FILTER.ALL"] a'));
    api.filterCategory = function (name) {
      element(by.cssContainingText('.filter-entry-options', name)).click();
    };

    api.list = {
      pageList: element.all(by.repeater('page in ctrl.currentPage.content')),
      pageName: function (elem) {
        return elem.$('.page-card-body .page-title.ng-binding');
      },
      followPage: function (pagename) {
        return api.list.subscribeButton(pagename, 'btn-default');
      },
      subscribeButtonSuccess: function (pagename) {
        return api.list.subscribeButton(pagename, 'btn-success');
      },
      subscribeButton: function (pagename, subscribeClass) {
        return element(by.cssContainingText('.panel-body', pagename)).$('.page-subscribe.' + subscribeClass);
      },
      paginationList: element.all(by.repeater('page in pages track by $index')),
      activePage: function () {
        return $('.pagination-page.active');
      },
      nextPage: $('a[ng-click="selectPage(page + 1, $event)"]')
    };
  }

  module.exports = PageList;

})();
