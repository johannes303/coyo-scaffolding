(function () {
  'use strict';

  function SelectLibrary() {
    var api = this;
    api.library1 = {
      libraryHome: $('a[ng-click="$ctrl.openHome()"]'), createFolder: $('a[ng-click="$ctrl.createFolder()"]')
    };

    api.documentLibrary = {
      myFiles: $('div[ng-click="$ctrl.openSender($ctrl.currentUser)"]'),
      homePage: $('.fl-table div:nth-child(2)'),
      pages: $('.fl-table div:nth-child(3)'),
      workspaces: $('.fl-table div:nth-child(4)')
    };
  }

  module.exports = SelectLibrary;

})();
