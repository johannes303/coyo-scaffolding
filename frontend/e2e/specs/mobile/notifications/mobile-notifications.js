(function () {
  'use strict';

  var login = require('../../login.page.js');
  var MobileNavigation = require('../mobile-navigation.page');

  describe('mobile notifications', function () {
    var mobileNavigation;

    beforeEach(function () {
      mobileNavigation = new MobileNavigation();

      login.loginDefaultUser();
    });

    it('open and close notifications', function () {
      mobileNavigation.notifications.open();
      expect(mobileNavigation.notifications.notificationsDialog.isPresent()).toBeTruthy();

      mobileNavigation.notifications.close();
      expect(mobileNavigation.notifications.notificationsDialog.isPresent()).toBeFalsy();
    });
  });

})();
