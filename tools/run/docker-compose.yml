version: '2'
services:
  coyo-lb:
    image: coyoapp/coyo-lb:${COYO_VERSION}
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "HTTP_BASIC_AUTH_USER=${COYO_MANAGEMENT_USER}"
      - "HTTP_BASIC_AUTH_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
    links:
      - coyo-frontend
      - coyo-backend
      - coyo-backup
      - coyo-push
      - coyo-docs
      - coyo-mq
      - coyo-stomp
      - coyo-kibana
      - coyo-grafana
    volumes:
      - ${COYO_CERT_FILE}:/certs/cert.pem
    ports:
      - "80:80"
      - "443:443"
      - "1936:1936"
      - "5601:5601"
      - "5602:5602"
      - "8083:8083"
      - "8084:8084"
      - "8161:8161"
      - "15672:15672"

  coyo-frontend:
    image: coyoapp/coyo-frontend:${COYO_VERSION}
    hostname: coyo-frontend
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "COYO_BACKEND_URL=https://${COYO_BACKEND_URL}"
      - "COYO_BACKEND_URL_STRATEGY=relative"
      - "VIRTUAL_HOST_WEIGHT=1"
      - "FORCE_SSL=TRUE"
      - "GZIP_COMPRESSION_TYPE=text/html text/css application/javascript"
      - "CONTENT_SECURITY_POLICY=${COYO_HTTP_CONTENT_SECURITY_POLICY}"
      - "TRANSPORT_SECURITY_INCLUDE_SUBDOMAINS=${COYO_HTTP_TRANSPORT_SECURITY_INCLUDE_SUBDOMAINS}"
    expose:
      - "80"

  coyo-backend:
    image: coyoapp/coyo-backend:${COYO_VERSION}
    hostname: coyo-backend
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "COYO_BACKEND_URL=https://${COYO_BACKEND_URL}"
      - "COYO_FRONTEND_URL=https://${COYO_FRONTEND_URL}"
      - "COYO_BASE_URL=https://${COYO_FRONTEND_URL}"
      - "COYO_JAVA_OPTS=${COYO_JAVA_OPTS}"
      - "COYO_DB_MAX_ACTIVE_CONNECTIONS=${COYO_DB_MAX_ACTIVE_CONNECTIONS}"
      - "COYO_SERVER_MAX_THREADS=${COYO_SERVER_MAX_THREADS}"
      - "COYO_MANAGEMENT_USER=${COYO_MANAGEMENT_USER}"
      - "COYO_MANAGEMENT_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "COYO_TENANT_STRATEGY=single"
      - "COYO_HOME=/var/lib/coyo/data"
      - "COYO_DB_HOST=coyo-db"
      - "COYO_DB_PORT=5432"
      - "COYO_DB_NAME=${COYO_DB_NAME}"
      - "COYO_DB_USER=${COYO_DB_USER}"
      - "COYO_DB_PASSWORD=${COYO_DB_PASSWORD}"
      - "COYO_ES_HOST=coyo-es"
      - "GZIP_COMPRESSION_TYPE=application/json"
      - "COYO_ES_PORT=9300"
      - "COYO_MONGODB_HOST=coyo-mongo"
      - "COYO_MONGODB_PORT=27017"
      - "COYO_MQ_HOST=coyo-mq"
      - "COYO_MQ_PORT=5672"
      - "COYO_MQ_USER=${COYO_MANAGEMENT_USER}"
      - "COYO_MQ_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "COYO_STOMP_HOST=coyo-stomp"
      - "COYO_STOMP_PORT=61613"
      - "COYO_STOMP_USER=${COYO_MANAGEMENT_USER}"
      - "COYO_STOMP_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "COYO_THEME_HOST=coyo-scss"
      - "COYO_THEME_PORT=3030"
      - "COYO_PROFILE=${COYO_PROFILE}"
      - "COYO_REDIS_HOST=coyo-redis"
      - "COYO_REDIS_PORT=6379"
      - "COYO_TIKA_HOST=coyo-tika"
      - "COYO_TIKA_PORT=9998"
      - "COYO_MAIL_FROM=${COYO_MAIL_FROM}"
      - "COYO_SESSION_COOKIE_NAME=${COYO_SESSION_COOKIE_NAME}"
      - "COYO_SESSION_REMEMBER_TIMEOUT=${COYO_SESSION_REMEMBER_TIMEOUT}"
      - "COYO_BACKEND_CLUSTER=${COYO_BACKEND_CLUSTER}"
      - "SPRING_MAIL_HOST=${COYO_MAIL_HOST}"
      - "SPRING_MAIL_USERNAME=${COYO_MAIL_USERNAME}"
      - "SPRING_MAIL_PASSWORD=${COYO_MAIL_PASSWORD}"
      - "SPRING_MAIL_PORT=${COYO_MAIL_PORT}"
      - "SPRING_MAIL_PROTOCOL=${COYO_MAIL_PROTOCOL}"
      - "VIRTUAL_HOST_WEIGHT=2"
      - "TRANSPORT_SECURITY_INCLUDE_SUBDOMAINS=${COYO_HTTP_TRANSPORT_SECURITY_INCLUDE_SUBDOMAINS}"
      - "COYO_PUSH_API_KEY=${COYO_PUSH_API_KEY}"
    links:
      - coyo-db
      - coyo-mq
      - coyo-stomp
      - coyo-es
      - coyo-redis
      - coyo-scss
      - coyo-mongo
      - coyo-tika
    volumes:
      - "${COYO_DATA_STORAGE}/backend:/var/lib/coyo/data"
      - "./coyo-backend.properties:/coyo/application-prod.properties"
    expose:
      - "8080"

  coyo-db:
    image: coyoapp/coyo-db:${COYO_VERSION}
    hostname: coyo-db
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "POSTGRES_DB=${COYO_DB_NAME}"
      - "POSTGRES_USER=${COYO_DB_USER}"
      - "POSTGRES_PASSWORD=${COYO_DB_PASSWORD}"
    volumes:
      - "${COYO_DATA_STORAGE}/db:/var/lib/postgresql/data"
    expose:
      - "5432"

  coyo-mq:
    image: coyoapp/coyo-mq:${COYO_VERSION}
    hostname: coyo-mq
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "RABBITMQ_DEFAULT_USER=${COYO_MANAGEMENT_USER}"
      - "RABBITMQ_DEFAULT_PASS=${COYO_MANAGEMENT_PASSWORD}"
    expose:
      - "4369"
      - "5671"
      - "5672"
      - "15671"
      - "15672"
      - "25672"

  coyo-stomp:
    image: coyoapp/activemq:5.15.2-2
    hostname: coyo-stomp
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "ACTIVEMQ_ADMIN_LOGIN=${COYO_MANAGEMENT_USER}"
      - "ACTIVEMQ_ADMIN_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "ACTIVEMQ_CONFIG_DEFAULTACCOUNT=false"
      - "ACTIVEMQ_CONFIG_MINMEMORY=${COYO_ACTIVEMQ_MIN_MEMORY}"
      - "ACTIVEMQ_CONFIG_MAXMEMORY=${COYO_ACTIVEMQ_MAX_MEMORY}"
      - "ACTIVEMQ_CONFIG_MAXCONNECTION=${COYO_ACTIVEMQ_MAX_CONNECTIONS}"
      - "ACTIVEMQ_CONFIG_HBGRACEPERIODMULTIPLIER=${COYO_ACTIVEMQ_HB_GRACE_PERIOD_MULTIPLIER}"
      - "ACTIVEMQ_LOGGER_LOGLEVEL=${COYO_ACTIVEMQ_LOGLEVEL}"
    expose:
      - "8161"
      - "61616"
      - "61613"

  coyo-es:
    image: coyoapp/coyo-es:${COYO_VERSION}
    hostname: coyo-es
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "ES_HEAP_SIZE=${ES_HEAP_SIZE}"
    volumes:
      - "${COYO_DATA_STORAGE}/es:/usr/share/elasticsearch/data"
    expose:
      - "9200"
      - "9300"

  coyo-docs:
    image: coyoapp/coyo-docs:${COYO_VERSION}
    hostname: coyo-docs
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "VIRTUAL_HOST_WEIGHT=3"
      - "FORCE_SSL=TRUE"

  coyo-redis:
    image: coyoapp/coyo-redis:${COYO_VERSION}
    hostname: coyo-redis
    labels:
      - "com.coyoapp.stack=main"
    expose:
      - "6379"

  coyo-scss:
    image: coyoapp/coyo-scss:${COYO_VERSION}
    hostname: coyo-scss
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "COYO_THEME_PORT=3030"
    expose:
      - "3030"

  coyo-mongo:
    image: mongo:3.2
    hostname: coyo-mongo
    labels:
      - "com.coyoapp.stack=main"
    volumes:
      - "${COYO_DATA_STORAGE}/mongo:/data/db"
    expose:
      - "27017"

  coyo-tika:
    image: coyoapp/coyo-tika:${COYO_VERSION}
    hostname: coyo-tika
    labels:
      - "com.coyoapp.stack=main"
    expose:
      - "9998"

  coyo-backup:
    image: coyoapp/coyo-backup:${COYO_VERSION}
    hostname: coyo-backup
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "COYO_BACKEND_URL=http://coyo-backend:8080"
      - "BACKUP_CRON=${BACKUP_CRON}"
      - "BACKUP_PATH=${BACKUP_PATH}"
      - "COYO_DB_HOST=coyo-db"
      - "COYO_DB_PORT=5432"
      - "COYO_DB_NAME=${COYO_DB_NAME}"
      - "COYO_DB_USER=${COYO_DB_USER}"
      - "COYO_DB_PASSWORD=${COYO_DB_PASSWORD}"
      - "COYO_FILE_HOST=coyo-mongo"
      - "COYO_FILE_PORT=${COYO_FILE_PORT}"
      - "COYO_FILE_DATABASE=${COYO_FILE_DATABASE}"
      - "COYO_BACKUP_JAVA_OPTS=${COYO_BACKUP_JAVA_OPTS}"
    links:
      - "coyo-backend"
      - "coyo-db"
      - "coyo-mongo"
    volumes:
      - "${COYO_DATA_STORAGE}/backup:${BACKUP_PATH}"
      - "./coyo-backup.properties:/coyo/application-prod.properties"
    expose:
      - "8083"

  coyo-push:
    image: coyoapp/coyo-push:${COYO_VERSION}
    hostname: coyo-push
    labels:
      - "com.coyoapp.stack=main"
    environment:
      - "COYO_MQ_HOST=coyo-mq"
      - "COYO_MQ_PORT=5672"
      - "COYO_MQ_USER=${COYO_MANAGEMENT_USER}"
      - "COYO_MQ_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "COYO_PUSH_THREAD_LIMIT=${COYO_PUSH_THREAD_LIMIT}"
      - "COYO_PUSH_JAVA_OPTS=${COYO_PUSH_JAVA_OPTS}"
    volumes:
      - "./coyo-push.properties:/coyo/application-prod.properties"
    expose:
      - "8084"

### ELK STACK: LOG AGGREGATION

  coyo-es-logs:
    image: coyoapp/coyo-es-logs:${COYO_VERSION}
    hostname: coyo-es-logs
    labels:
      - "com.coyoapp.stack=monitoring"
    environment:
      - "xpack.security.enabled=false"
      - "ES_JAVA_OPTS=-Xms512m -Xmx${ES_LOGS_HEAP_SIZE}"
    volumes:
      - "${COYO_DATA_STORAGE}/es-logs:/usr/share/elasticsearch/data"

  coyo-kibana:
    image: coyoapp/coyo-kibana:${COYO_VERSION}
    hostname: coyo-kibana
    labels:
      - "com.coyoapp.stack=monitoring"
    environment:
      - "ELASTICSEARCH_URL=http://elasticsearch:9200"
      - "xpack.security.enabled=false"
      - "xpack.monitoring.report_stats=false"
    expose:
      - "5601"
    links:
      - coyo-es-logs:elasticsearch

  coyo-logstash:
    image: coyoapp/coyo-logstash:${COYO_VERSION}
    hostname: coyo-logstash
    labels:
      - "com.coyoapp.stack=monitoring"
    environment:
      - "xpack.security.enabled=false"
    expose:
      - "4560"
    links:
      - coyo-es-logs:elasticsearch

  coyo-logspout:
    image: coyoapp/coyo-logspout:${COYO_VERSION}
    hostname: coyo-logspout
    labels:
      - "com.coyoapp.stack=monitoring"
    environment:
      ROUTE_URIS: "logstash+tcp://logstash:4560"
      LOGSPOUT: 'ignore'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    links:
      - coyo-logstash:logstash
    tty: true
    stdin_open: true
    restart: always

### TIG STACK: METRIC AGGREGATION

  coyo-telegraf:
    image: coyoapp/coyo-telegraf:${COYO_VERSION}
    hostname: coyo-telegraf
    labels:
      - "com.coyoapp.stack=monitoring"
    links:
      - coyo-influxdb
      - coyo-lb
      - coyo-backend
      - coyo-backup
      - coyo-frontend
      - coyo-db
      - coyo-mq
      - coyo-redis
      - coyo-es
      - coyo-tika
      - coyo-stomp
    expose:
      - "8092/udp"
      - "8094"
      - "8125/udp"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    environment:
      - "HAPROXY_USER=${COYO_MANAGEMENT_USER}"
      - "HAPROXY_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "POSTGRES_DB=${COYO_DB_NAME}"
      - "POSTGRES_USER=${COYO_DB_USER}"
      - "POSTGRES_PASSWORD=${COYO_DB_PASSWORD}"
      - "BACKEND_USER=${COYO_MANAGEMENT_USER}"
      - "BACKEND_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "STOMP_USER=${COYO_MANAGEMENT_USER}"
      - "STOMP_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "RABBITMQ_USER=${COYO_MANAGEMENT_USER}"
      - "RABBITMQ_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"

  coyo-influxdb:
    image: coyoapp/coyo-influxdb:${COYO_VERSION}
    hostname: coyo-influxdb
    labels:
      - "com.coyoapp.stack=monitoring"
    volumes:
      - "${COYO_DATA_STORAGE}/influxdb:/var/lib/influxdb"
    environment:
      - "INFLUXDB_DATA_MAX_VALUES_PER_TAG=0"
    expose:
      - "8086"

  coyo-grafana:
    image: coyoapp/coyo-grafana:${COYO_VERSION}
    hostname: coyo-grafana
    labels:
      - "com.coyoapp.stack=monitoring"
    expose:
      - "3000"
    environment:
      - "GF_INSTALL_PLUGINS=grafana-piechart-panel,grafana-clock-panel,briangann-gauge-panel,natel-plotly-panel,grafana-simple-json-datasource"
      - "GF_SERVER_ROOT_URL=https://${COYO_FRONTEND_URL}:5602"
      - "GF_SECURITY_ADMIN_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "GF_SECURITY_ADMIN_USER=${COYO_MANAGEMENT_USER}"
    links:
      - coyo-influxdb:influxdb
